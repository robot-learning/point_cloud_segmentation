# Point Cloud Segmentation

This repo is for using RANSAC to subtract the table and background from an image and pointcloud.

## Installation

**TODO** This section could use some more details on how to get dependencies setup correctly.

This needs ROS Melodic (other ROS versions may work), PCL, and opecv.

You will need the [ll4ma_util](https://bitbucket.org/robot-learning/ll4ma_util/src/main/) package to use the visualizer:

    git clone git@bitbucket.org:robot-learning/ll4ma_util.git
    
Known issues:

    AttributeError: 'module' object has no attribute 'Interpreter' 

`pip install empy` (not `em`) https://github.com/ros/genmsg/issues/63

## Filter Region Visualizer
You must set the boundaries of the region over which the segmentation algorithm will operate. You may have to adjust this each time your camera moves or you need to segement over a different region of the space. A visualizer is provided to make this easier.

![Visualizer](imgs/segmentation_visualizer.png)

To use the visualizer, make sure the `use_region_visualizer` flag is set to true for `table_obj_segmenter.launch`, and you should pass in the name of a YAML file to save the values so they will persist between different sessions, i.e.

    roslaunch point_cloud_segmentation table_obj_segmenter.launch use_region_visualizer:=true yaml_config:=/home/user/path/to/config/my_config.yaml
	
It's best if you include the segmentation launch in your own pipeline's launch file so you can customize the args as you need them without having to pass them at the commandline each time, for example:

```
  <include file="$(find point_cloud_segmentation)/launch/table_obj_segmenter.launch">
    <arg name="real_camera"            value="true"/>
    <arg name="real_pcd_topic"         value="/kinect2/qhd/points"/>
    <arg name="real_sd_pcd_topic"      value="/kinect2/sd/points"/>
    <arg name="real_rgb_topic"         value="/kinect2/qhd/image_color_rect"/>
    <arg name="real_sd_rgb_topic"      value="/kinect2/sd/image_color_rect"/>
    <arg name="real_depth_topic"       value="/kinect2/qhd/image_depth_rect"/>
    <arg name="real_sd_depth_topic"    value="/kinect2/sd/image_depth_rect"/>

    <arg name="visualize_bounding_box" value="false"/>
    <arg name="visualize_clusters"     value="false"/>
    <arg name="yaml_config"            value="$(find ll4ma_isaacgym)/config/pcd_segmentation.yaml"/>
  </include>
```

See [this video](https://youtu.be/wAG2wvpesjU) for a short demo of how the visualizer works. Once the node is running, you'll need to add these to your rviz config (note `Filter` is the node's namespace, which can be set with `node_name` arg to `table_obj_segmenter.launch`):

1. Add > By display type > Group
2. Add > By topic > MarkerArray (/Filter/filter_limit_region)
3. Add > By topic > InteractiveMarkers (/Filter/update)
4. In left rviz panel, drag and drop the MarkerArray and InteractiveMarkers icons into the created Group. This allows you to simultaneously enable/disable the visualizations. You can rename these as desired.

Once these components are active in rviz, you can drag the arrows around to change the dimensions of the region, you should probably add a visualization of your pointcloud also so you can see where the region is active. When you're happy with the values, right-click any arrow and click "Save". Now if you run segmentation it will use the selected region. You can directly call the service by right-clicking an arrow and clicking "Run Segmentation". If you visualize the point cloud topics offered by the segmentation service you will see the segmented object and table on two different topics to verify it worked.
