#!/usr/bin/env python

import os
import rospy
from enum import Enum
from geometry_msgs.msg import Point
from visualization_msgs.msg import (
    MarkerArray, InteractiveMarker, InteractiveMarkerControl,
    InteractiveMarkerFeedback, InteractiveMarkerUpdate
)
from std_srvs.srv import Trigger, TriggerRequest
from interactive_markers.interactive_marker_server import InteractiveMarkerServer
from interactive_markers.menu_handler import MenuHandler

from point_cloud_segmentation.srv import SegmentGraspObject, SegmentGraspObjectRequest
from ll4ma_util import ros_util, file_util
from ll4ma_util.ros_util import get_marker_msg


class Interaction(Enum):
    """
    Enum for interaction types (plus and minus arrows for eacy xyz dimension)
    """
    MOVE_X_PLUS = 0
    MOVE_X_MINUS = 1
    MOVE_Y_PLUS = 2
    MOVE_Y_MINUS = 3
    MOVE_Z_PLUS = 4
    MOVE_Z_MINUS = 5


class FilterLimitVisualizer:
    """
    Visualizes the region over which pointcloud segmentation will be performed,
    allowing the user to change the region using interactive markers in rviz.

    Interactive arrow markers are used to set the pose and scale of a Marker cubiod
    that visualizes the region over which pointcloud segmentation will be performed.
    
    Note that the box region is set with origin in the centroid of the cuboid, so
    movement of an arrow will trigger a change in both the origin of the box and the
    scale of the box in that dimension (e.g. move plus-x-arrow in positive direction
    by value of M, increase x-dimension scale by M/2 and decrease x-dimension position
    by M/2). This gives the appearance of the box staying stationary while the box
    size is increased proportional to the amount the user moves the arrow.
    """

    def __init__(self, namespace="Filter", yaml_filename="", rate=100,
                 arrow_length=0.5, arrow_params=[0.1, 0.2, 0.2]):
        """
        Args:
            namespace (str): Namespace to read/write ROS parameter server values from/to
            yaml_filename (str): YAML filename to save/load config values to/from
            rate (float): Rate at which this node will run
            arrow_length (float): Length of the arrow head for the interactive markers
            arrow_params (list): List of 3 values specifying the arrow shaft diameter, head 
                                 x-width, and head y-width for the arrow interactive markers.
        """
        self.namespace = namespace
        self.yaml_filename = yaml_filename
        self.rate = rospy.Rate(rate)
        self.arrow_length = arrow_length
        self.arrow_params = arrow_params

        self.frame = rospy.get_param("/{}/workspace_frame".format(namespace), "world")

        self.prev_values = {k: 0.0 for k in Interaction}
        self.poses = {k: None for k in Interaction}

        self.marker_array = MarkerArray()
        self.marker = ros_util.get_marker_msg(alpha=0.8, color='dodgerblue', frame_id=self.frame)
        '''
        self.text = ros_util.get_marker_msg(scale=[0.1]*3, shape='text', color='white',
                                            marker_id=2), text=("Right-click any arrow and select "
                                                               "'Save' to save current values"))
        self.text.pose.position.z = 1.
        '''
        self.marker_pub = rospy.Publisher("/{}/filter_limit_region".format(self.namespace),
                                          MarkerArray, queue_size=1)

        self.update_pub = rospy.Publisher("/{}/update".format(self.namespace),
                                          InteractiveMarkerUpdate, queue_size=1)
        self.has_subscribers = False
        
    def run(self):
        """
        Main entrypoint for running this node. Monitors for user interactions with the
        rviz interactive markers and updates the region accordingly.
        """
        self._get_from_param_server()

        if self.yaml_filename and os.path.exists(self.yaml_filename):
            saved_values = file_util.load_yaml(self.yaml_filename)
            self.limits.update(saved_values)
            self._save_to_param_server()

        self.marker.scale.x = self.limits['max_x'] - self.limits['min_x']
        self.marker.scale.y = self.limits['max_y'] - self.limits['min_y']
        self.marker.scale.z = self.limits['max_z'] - self.limits['min_z']
        
        self.marker.pose.position.x = self.marker.scale.x / 2. + self.limits['min_x']
        self.marker.pose.position.y = self.marker.scale.y / 2. + self.limits['min_y']
        self.marker.pose.position.z = self.marker.scale.z / 2. + self.limits['min_z']

        self._create_markers()

        rospy.loginfo("Filter limit visualizer is active...")
        while not rospy.is_shutdown():
            # This is necessary for when the user hides the interactive markers and then
            # re-enables them in rviz, you have to change the pose of the actual markers
            # or it will re-enable them in their original pose instead of the changed ones
            if not self.has_subscribers and self.update_pub.get_num_connections() > 0:
                for interaction in Interaction:
                    self.markers[interaction].controls[0].markers[0].pose = self.poses[interaction]
                self.has_subscribers = True
            elif self.update_pub.get_num_connections() == 0:
                self.has_subscribers = False
            self._cache_marker_poses()
                
            self.marker_array.markers = [self.marker]#, self.text]
            self.marker_pub.publish(self.marker_array)
            self.rate.sleep()

    def _create_markers(self):
        """
        Creates the arrow markers that are the main source of user interaction.
        """
        self.markers = {}
        
        x_plus_marker = InteractiveMarker()
        x_plus_marker.header.frame_id = self.frame
        x_plus_marker.scale = 0.5
        x_plus_marker.name = Interaction.MOVE_X_PLUS.name
        control = InteractiveMarkerControl()
        control.orientation.x = 0.70710678
        control.orientation.y = 0
        control.orientation.z = 0
        control.orientation.w = 0.70710678
        control.name = Interaction.MOVE_X_PLUS.name
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        start = Point(self.marker.pose.position.x + self.marker.scale.x / 2.,
                      self.marker.pose.position.y,
                      self.marker.pose.position.z)
        end = Point(start.x + self.arrow_length, start.y, start.z)
        marker = get_marker_msg(scale=self.arrow_params, shape='arrow', color='red', alpha=0.99)
        marker.points = [start, end]
        control.markers.append(marker)
        x_plus_marker.controls.append(control)
        self.markers[Interaction.MOVE_X_PLUS] = x_plus_marker

        x_minus_marker = InteractiveMarker()
        x_minus_marker.header.frame_id = self.frame
        x_minus_marker.scale = 0.5
        x_minus_marker.name = Interaction.MOVE_X_MINUS.name
        control = InteractiveMarkerControl()
        control.orientation.x = 0.70710678
        control.orientation.y = 0
        control.orientation.z = 0
        control.orientation.w = 0.70710678
        control.name = Interaction.MOVE_X_MINUS.name
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        start = Point(self.marker.pose.position.x - self.marker.scale.x / 2.,
                      self.marker.pose.position.y,
                      self.marker.pose.position.z)
        end = Point(start.x - self.arrow_length, start.y, start.z)
        marker = get_marker_msg(scale=self.arrow_params, shape='arrow', color='red', alpha=0.99)
        marker.points = [start, end]
        control.markers.append(marker)
        x_minus_marker.controls.append(control)
        self.markers[Interaction.MOVE_X_MINUS] = x_minus_marker
        
        y_plus_marker = InteractiveMarker()
        y_plus_marker.header.frame_id = self.frame
        y_plus_marker.scale = 0.5
        y_plus_marker.name = Interaction.MOVE_Y_PLUS.name
        control = InteractiveMarkerControl()
        control.orientation.x = 0
        control.orientation.y = 0
        control.orientation.z = 0.70710678
        control.orientation.w = 0.70710678
        control.name = Interaction.MOVE_Y_PLUS.name
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        start = Point(self.marker.pose.position.x,
                      self.marker.pose.position.y + self.marker.scale.y / 2.,
                      self.marker.pose.position.z)
        end = Point(start.x, start.y + self.arrow_length, start.z)
        marker = get_marker_msg(scale=self.arrow_params, shape='arrow', color='green', alpha=0.99)
        marker.points = [start, end]
        control.markers.append(marker)
        y_plus_marker.controls.append(control)
        self.markers[Interaction.MOVE_Y_PLUS] = y_plus_marker

        y_minus_marker = InteractiveMarker()
        y_minus_marker.header.frame_id = self.frame
        y_minus_marker.scale = 0.5
        y_minus_marker.name = Interaction.MOVE_Y_MINUS.name
        control = InteractiveMarkerControl()
        control.orientation.x = 0
        control.orientation.y = 0
        control.orientation.z = -0.70710678
        control.orientation.w = 0.70710678
        control.name = Interaction.MOVE_Y_MINUS.name
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        start = Point(self.marker.pose.position.x,
                      self.marker.pose.position.y - self.marker.scale.y / 2.,
                      self.marker.pose.position.z)
        end = Point(start.x, start.y - self.arrow_length, start.z)
        marker = get_marker_msg(scale=self.arrow_params, shape='arrow', color='green', alpha=0.99)
        marker.points = [start, end]
        control.markers.append(marker)
        y_minus_marker.controls.append(control)
        self.markers[Interaction.MOVE_Y_MINUS] = y_minus_marker

        z_plus_marker = InteractiveMarker()
        z_plus_marker.header.frame_id = self.frame
        z_plus_marker.scale = 0.5
        z_plus_marker.name = Interaction.MOVE_Z_PLUS.name
        control = InteractiveMarkerControl()
        control.orientation.x = 0
        control.orientation.y = 0.70710678
        control.orientation.z = 0
        control.orientation.w = 0.70710678
        control.name = Interaction.MOVE_Z_PLUS.name
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        start = Point(self.marker.pose.position.x,
                      self.marker.pose.position.y,
                      self.marker.pose.position.z + self.marker.scale.z / 2.)
        end = Point(start.x, start.y, start.z + self.arrow_length)
        marker = get_marker_msg(scale=self.arrow_params, shape='arrow', color='blue', alpha=0.99)
        marker.points = [start, end]
        control.markers.append(marker)
        z_plus_marker.controls.append(control)
        self.markers[Interaction.MOVE_Z_PLUS] = z_plus_marker

        z_minus_marker = InteractiveMarker()
        z_minus_marker.header.frame_id = self.frame
        z_minus_marker.scale = 0.5
        z_minus_marker.name = Interaction.MOVE_Z_MINUS.name
        control = InteractiveMarkerControl()
        control.orientation.x = 0
        control.orientation.y = -0.70710678
        control.orientation.z = 0
        control.orientation.w = 0.70710678
        control.name = Interaction.MOVE_Z_MINUS.name
        control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
        start = Point(self.marker.pose.position.x,
                      self.marker.pose.position.y,
                      self.marker.pose.position.z - self.marker.scale.z / 2.)
        end = Point(start.x, start.y, start.z - self.arrow_length)
        marker = get_marker_msg(scale=self.arrow_params, shape='arrow', color='blue', alpha=0.99)
        marker.points = [start, end]
        control.markers.append(marker)
        z_minus_marker.controls.append(control)
        self.markers[Interaction.MOVE_Z_MINUS] = z_minus_marker
        
        self.server = InteractiveMarkerServer(self.namespace)
        for marker in self.markers.values():
            self.server.insert(marker, self._process_marker_feedback)
        self.server.applyChanges()

        self.menu_handler = MenuHandler()
        self.menu_handler.insert("Save", callback=self._save_callback)
        self.menu_handler.insert("Run Segmentation", callback=self._run_segmentation)
        for interaction in Interaction:
            self.menu_handler.apply(self.server, interaction.name)
            
    def _process_marker_feedback(self, feedback):
        """
        Processes the feedback from user interactions with the interactive markers in rviz.
        Primarily updates the poses of the markers and the dimensions of the box visualizing
        the selected region.

        Args:
            feedback (InteractiveMarkerFeedback): Feedback from interaction
        """
        if feedback.event_type == InteractiveMarkerFeedback.POSE_UPDATE:
            interaction = Interaction[feedback.control_name]
            if interaction in [Interaction.MOVE_X_PLUS, Interaction.MOVE_X_MINUS]:
                self._change_x_marker(interaction, feedback)
            elif interaction in [Interaction.MOVE_Y_PLUS, Interaction.MOVE_Y_MINUS]:
                self._change_y_marker(interaction, feedback)
            elif interaction in [Interaction.MOVE_Z_PLUS, Interaction.MOVE_Z_MINUS]:
                self._change_z_marker(interaction, feedback)        
        self.server.applyChanges()

    def _change_x_marker(self, interaction, feedback):
        """
        Utility function for changing the pose of the X-dimension markers. Changes in
        X-dimension markers also affect the Y- and Z-dimension markers.

        Args:
            interaction (Interaction): Interaction type
            feedback (InteractiveMarkerFeedback): Feedback from interaction
        """
        delta = feedback.pose.position.x - self.prev_values[interaction]
        if interaction == Interaction.MOVE_X_PLUS:
            self.marker.scale.x += delta
        else:
            self.marker.scale.x -= delta
        half_delta = delta / 2.
        self.marker.pose.position.x += half_delta
        self.prev_values[interaction] = feedback.pose.position.x

        self._update_position(Interaction.MOVE_Y_PLUS, half_delta, 'x')
        self._update_position(Interaction.MOVE_Y_MINUS, half_delta, 'x')
        self._update_position(Interaction.MOVE_Z_PLUS, half_delta, 'x')
        self._update_position(Interaction.MOVE_Z_MINUS, half_delta, 'x')

    def _change_y_marker(self, interaction, feedback):
        """
        Utility function for changing the pose of the Y-dimension markers. Changes in
        Y-dimension markers also affect the X- and Z-dimension markers.

        Args:
            interaction (Interaction): Interaction type
            feedback (InteractiveMarkerFeedback): Feedback from interaction
        """
        delta = feedback.pose.position.y - self.prev_values[interaction]
        if interaction == Interaction.MOVE_Y_PLUS:
            self.marker.scale.y += delta
        else:
            self.marker.scale.y -= delta
        half_delta = delta / 2.
        self.marker.pose.position.y += half_delta
        self.prev_values[interaction] = feedback.pose.position.y

        self._update_position(Interaction.MOVE_X_PLUS, half_delta, 'y')
        self._update_position(Interaction.MOVE_X_MINUS, half_delta, 'y')
        self._update_position(Interaction.MOVE_Z_PLUS, half_delta, 'y')
        self._update_position(Interaction.MOVE_Z_MINUS, half_delta, 'y')
        
    def _change_z_marker(self, interaction, feedback):
        """
        Utility function for changing the pose of the Z-dimension markers. Changes in
        Z-dimension markers also affect the X- and Y-dimension markers.

        Args:
            interaction (Interaction): Interaction type
            feedback (InteractiveMarkerFeedback): Feedback from interaction
        """
        delta = feedback.pose.position.z - self.prev_values[interaction]
        if interaction == Interaction.MOVE_Z_PLUS:
            self.marker.scale.z += delta
        else:
            self.marker.scale.z -= delta
        half_delta = delta / 2.
        self.marker.pose.position.z += half_delta
        self.prev_values[interaction] = feedback.pose.position.z

        self._update_position(Interaction.MOVE_X_PLUS, half_delta, 'z')
        self._update_position(Interaction.MOVE_X_MINUS, half_delta, 'z')
        self._update_position(Interaction.MOVE_Y_PLUS, half_delta, 'z')
        self._update_position(Interaction.MOVE_Y_MINUS, half_delta, 'z')

    def _update_position(self, interaction, value, dim):
        """
        Update the specified position dimension of the specified arrow marker.
        """
        if dim not in ['x', 'y', 'z']:
            raise ValueError("Can only set 'dim' to 'x', 'y', or 'z'")
        pose = self.server.get(interaction.name).pose
        current = getattr(pose.position, dim)
        setattr(pose.position, dim, current + value)
        self.server.setPose(interaction.name, pose)

    def _cache_marker_poses(self):
        """
        Caches the current pose of the markers. This is necessary for resetting the
        arrow markers to the correct pose after a user disables and then re-enables
        the markers in rviz.
        """
        for interaction in Interaction:
            self.poses[interaction] = self.server.get(interaction.name).pose
        
    def _compute_current_limits(self):
        """
        Computes the current selected region limits based on the marker dimensions.
        """
        pos = self.marker.pose.position
        scale = self.marker.scale
        self.limits['min_x'] = pos.x - scale.x / 2.
        self.limits['max_x'] = pos.x + scale.x / 2.
        self.limits['min_y'] = pos.y - scale.y / 2.
        self.limits['max_y'] = pos.y + scale.y / 2.
        self.limits['min_z'] = pos.z - scale.z / 2.
        self.limits['max_z'] = pos.z + scale.z / 2.
        
    def _get_from_param_server(self):
        """
        Retrieves the current region limit values from the ROS parameter server.
        """
        self.limits = {
            'min_x': rospy.get_param("/{}/min_x".format(self.namespace)),
            'max_x': rospy.get_param("/{}/max_x".format(self.namespace)),
            'min_y': rospy.get_param("/{}/min_y".format(self.namespace)),
            'max_y': rospy.get_param("/{}/max_y".format(self.namespace)),
            'min_z': rospy.get_param("/{}/min_z".format(self.namespace)),
            'max_z': rospy.get_param("/{}/max_z".format(self.namespace))
        }

    def _save_to_param_server(self):
        """
        Saves the current region limit values set with the rviz marker visualization
        to the ROS parameter server so the segmentation algorithm can access them.
        """
        rospy.set_param("/{}/min_x".format(self.namespace), self.limits['min_x'])
        rospy.set_param("/{}/max_x".format(self.namespace), self.limits['max_x'])
        rospy.set_param("/{}/min_y".format(self.namespace), self.limits['min_y'])
        rospy.set_param("/{}/max_y".format(self.namespace), self.limits['max_y'])
        rospy.set_param("/{}/min_z".format(self.namespace), self.limits['min_z'])
        rospy.set_param("/{}/max_z".format(self.namespace), self.limits['max_z'])

    def _save_to_file(self):
        """
        Saves the current region limit values to disk in a YAML file so they can be
        later read from disk to initialize the region with the previously set values
        on startup.
        """
        if not self.yaml_filename:
            rospy.logwarn("Tried to save limit params but no save filename has "
                          "been set. Not saving any data.")
        else:
            file_util.create_dir(os.path.dirname(self.yaml_filename))
            file_util.save_yaml(self.limits, self.yaml_filename)

    def _save_callback(self, feedback):
        """
        Callback function for user doing Right-click > Save on any of the arrows.
        """
        self._compute_current_limits()
        self._save_to_param_server()
        self._save_to_file()

        # Have the segmentation node update its values from the param server
        ros_util.call_service(TriggerRequest(), "refresh_param_server_values", Trigger)

    def _run_segmentation(self, feedback):
        """
        Callback function for user doing Right-click > Run Segmentation on any of the arrows.
        """
        req = SegmentGraspObjectRequest()
        resp, _ = ros_util.call_service(req, "/object_segmenter", SegmentGraspObject)


if __name__ == '__main__':
    rospy.init_node('set_filter_limits')

    namespace = rospy.get_param("~namespace", "pcd_filter_ransac_seg_node")
    yaml_filename = rospy.get_param("~yaml_filename", "config.yaml")
    rate = rospy.get_param("~rate", 100)
    arrow_length = rospy.get_param("~arrow_length", 0.5)
    arrow_params = rospy.get_param("~arrow_params", [0.1, 0.2, 0.2])

    visualizer = FilterLimitVisualizer(namespace, yaml_filename, rate, arrow_length, arrow_params)
    visualizer.run()
