'''
This test asserts that c++ code is correctly translated to Python.
'''

import numpy as np
import rospy
import sensor_msgs.point_cloud2 as pc2

from pca_bounding_box import compute_bounding_box_pca
from point_cloud_segmentation.srv import *
from scipy.spatial.transform import Rotation as R
from std_msgs.msg import Header
from sensor_msgs.msg import PointField

FIELDS_XYZ = [
    PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
    PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
    PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
]

def call_service(name, request, message_type, wait_for_service=False):
    response = None
    try:
        if wait_for_service:
            rospy.loginfo("Waiting for service " + name)
            wait_for_service(name)
        proxy = rospy.ServiceProxy(name, message_type)
        response = proxy(request)
    except Exception as e:
        rospy.logerr("Service " + name + " call failed: " + str(e))
        return response
    rospy.loginfo("Service " + name + " call finished successfully.")
    return response

def test(point_cloud, debug):
    # python 
    T_python, dims_python = compute_bounding_box_pca(point_cloud)
    if debug:
        print('T python: ', T_python)
        print('dims python: ', dims_python)
        print()

    # c++
    req = SegmentGraspObjectRequest()
    req.obj_world_cloud = pc2.create_cloud(Header(), FIELDS_XYZ, point_cloud)
    resp = call_service('/bounding_box_PCA', req, SegmentGraspObject)
    T_legacy = np.eye(4)
    T_legacy[:3,:3] = R.from_quat([resp.obj.pose.orientation.x,
                                  resp.obj.pose.orientation.y,
                                  resp.obj.pose.orientation.z,
                                  resp.obj.pose.orientation.w]).as_matrix()
    T_legacy[0,3] = resp.obj.pose.position.x
    T_legacy[1,3] = resp.obj.pose.position.y
    T_legacy[2,3] = resp.obj.pose.position.z

    dims_legacy = np.array([resp.obj.width, resp.obj.height, resp.obj.depth])
 
    if debug:
        print('T legacy: ', T_legacy)
        print('dims legacy: ', dims_legacy)
        print()

    # check that positions are the same
    assert np.allclose(T_legacy[:3,3], T_python[:3,3], atol=1e-3)

    # check that dimensions are the same
    assert np.allclose(dims_legacy, dims_python, atol=1e-3)

    # check that the eigenvectors are same (up to orientation)
    for i in range(3):
        eigenvector_legacy = T_legacy[:,i]
        eigenvector_python = T_python[:,i]
        assert np.allclose(eigenvector_legacy, eigenvector_python, atol=1e-3) or \
               np.allclose(-1 * eigenvector_legacy, eigenvector_python, atol=1e-3)

for _ in range(50):
    point_cloud = np.random.rand(100, 3)  # Replace with your actual point cloud data
    test(point_cloud, debug=False)
print("All tests passed successfully!")

