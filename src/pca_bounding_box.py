import numpy as np

def compute_bounding_box_pca(point_cloud, debug=False):
    '''
    point_cloud: Nx3 numpy array
    '''
    mean, eigenvalues, eigenvectors = compute_pca(point_cloud)
    eigenspace_data = transform_to_eigenspace(point_cloud, mean, eigenvectors)
    x_width = abs(max(eigenspace_data[:,0]) - min(eigenspace_data[:,0]))
    y_width = abs(max(eigenspace_data[:,1]) - min(eigenspace_data[:,1]))
    z_width = abs(max(eigenspace_data[:,2]) - min(eigenspace_data[:,2]))
    dims = [x_width, y_width, z_width]

    box_mean_eigenspace = 0.5 * np.array([
        min(eigenspace_data[:,0]) + max(eigenspace_data[:,0]),
        min(eigenspace_data[:,1]) + max(eigenspace_data[:,1]),
        min(eigenspace_data[:,2]) + max(eigenspace_data[:,2])])

    # in object space
    position = eigenvectors @ box_mean_eigenspace + mean
    # this is unaligned orientation
    orientation = eigenvectors

    if debug:
        print("Mean:\n", mean)
        print("Eigenvalues:\n", eigenvalues)
        print("Eigenvectors:\n", eigenvectors)

        print("position:\n", position)
        print("orientation:\n", orientation)
        print("x width: ", x_width)
        print("y width: ", y_width)
        print("z width: ", z_width)
    T = np.eye(4)
    T[:3,:3] = orientation
    T[:3, 3] = position
    return T, dims

def compute_pca(point_cloud):

    # compute the mean
    mean = np.mean(point_cloud, axis=0)
    
    # center the data
    centered_data = point_cloud - mean
    
    # compute the covariance matrix
    covariance_matrix = np.cov(centered_data, rowvar=False)
    
    # perform eigen decomposition
    # eigenvectors are column vectors here
    eigenvalues, eigenvectors = np.linalg.eigh(covariance_matrix)
    
    # sort eigenvalues and eigenvectors
    sort_indices = np.argsort(eigenvalues)[::-1]
    eigenvalues = eigenvalues[sort_indices]
    eigenvectors = eigenvectors[:, sort_indices]

    # ensure the coordinate system is right handed
    eigenvectors[:,2] = np.cross(eigenvectors[:,0], eigenvectors[:,1])
  
    return mean, eigenvalues, eigenvectors

def transform_to_eigenspace(point_cloud, mean, eigenvectors):
    # center the data
    centered_data = point_cloud - mean
    
    # transform to eigenspace
    # TODO maybe transpose here on eigenvectors
    eigenspace_data = centered_data @ eigenvectors
    
    return eigenspace_data

# Example usage
# point_cloud = np.random.rand(100, 3)  # Replace with your actual point cloud data
# T, dims = compute_bounding_box_pca(point_cloud)

