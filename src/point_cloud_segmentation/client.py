#!/usr/bin/env python

import rospy
import ros_numpy


import numpy as np
import torch
import os
import ctypes
import struct
from sensor_msgs import point_cloud2
from point_cloud_segmentation.srv import *


def segment_object_client(pc_topic='', min_x=None, min_y=None, min_z=None, max_x=None, max_y=None, max_z=None):
    '''
    segment object out from the image and assign to self.object_segment_response
    '''
    rospy.loginfo('Waiting for service object_segmenter.')
    rospy.wait_for_service('object_segmenter')
    rospy.loginfo('Calling service object_segmenter.')
    if min_x is not None:
        rospy.set_param('/pcd_filter_ransac_seg_node/min_x', min_x)
    if min_y is not None:
        rospy.set_param('/pcd_filter_ransac_seg_node/min_y', min_y)
    if min_z is not None:
        rospy.set_param('/pcd_filter_ransac_seg_node/min_z', min_z)
    if max_x is not None:
        rospy.set_param('/pcd_filter_ransac_seg_node/max_x', max_x)
    if max_y is not None:
        rospy.set_param('/pcd_filter_ransac_seg_node/max_y', max_y)
    if max_z is not None:
        rospy.set_param('/pcd_filter_ransac_seg_node/max_z', max_z)
    try:
        object_segment_proxy = rospy.ServiceProxy('object_segmenter', SegmentGraspObject)
        object_segment_request = SegmentGraspObjectRequest()
        object_segment_request.client_cloud_path = pc_topic
        object_segment_response = object_segment_proxy(object_segment_request)
    except Exception as e:
        rospy.loginfo('Service object_segmenter call failed: %s'%e)
        import pdb; pdb.set_trace()
    rospy.loginfo('Service object_segmenter is executed.')
    if not object_segment_response.object_found:
        rospy.logerr('No object found from segmentation!')
        return False
    return object_segment_response.obj



if __name__ == '__main__':
    rospy.init_node('segment_client_baos')
    pcpath = os.path.expanduser('~/data/cleaner_pc.pcd')
    pc2 = segment_object_client(pcpath, min_x=0.6, min_y=-0.3, min_z=0.58, max_x=1.0, max_y=0.3, max_z=0.85)
    # pc = ros_numpy.numpify(pc2)
    # # print(pc.shape)
    # # print(pc[100][100])
    # # print(pc[:][:]['x'].shape)
    # current_points = np.zeros((pc.shape[0], pc.shape[1], 3))
    # current_points[:,:,0] = pc[:][:]['x']
    # current_points[:,:,1] = pc[:][:]['y']
    # current_points[:,:,2] = pc[:][:]['z']
    # current_points[np.isnan(current_points)]=  0
    # print(pc2.cloud.data)
    numpy_pc = point_cloud2.read_points(pc2.cloud, field_names=['x', 'y', 'z'])
    # # print(numpy_pc.obj)
    # for stuff in numpy_pc:
    #     print(stuff)

    numpy_pc = np.array([item for item in numpy_pc])
    print(numpy_pc.shape)
